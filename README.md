
# Documentation Project

> Here I will write about the challenge proposed by the Company Group A Education. Let's go team.


## BACK-END (WEB SERVICE API)

```
  ARCHITECTURE
```

| Layer    | Description                           |
| :----------  | :---------------------------------- |
| `API`  | **Responsible** for receiving customer information (front-end) |
| `APLICATION`  | **Responsible** for identifying and directing which services should be used  |
| `DOMAIN`  | **Responsible** for the domain rule |
| `INFRA`  | **Responsible** for system configuration (database, dependency injection, etc)|
| `TEST`  | **Responsible** for test the system |

**EXPLAINING:** 
> After a good time I was studying about architecture and design project (SQRS and DDD).
> Finaly, I found a soluction that helped me to create a structure.


```
  1) API
```

| Type    | Name  | Version |
| :-----:  | :---------: | :---: |
| `Target Framework`  | **.net Core** | 5.0 |
| `Package Reference`  | **AutoMapper** | 11.0.1 |
| `Package Reference`  | **Swashbuckle (Swagger)** | 6.2.3 |
| `Docker Default Target OS`  | **Docker in Linux** ✅ | 3.4 |

```
  2) APLICATION
```

| Type    | Name  | Version |
| :-----:  | :---------: | :---: |
| `Target Framework`  | **net Standard** | 2.1 |
| `Package Reference`  | **MediatR** | 10.0.1 |

```
  3) DOMAIN
```

| Type    | Name  | Version |
| :-----:  | :---------: | :---: |
| `Target Framework`  | **net Standard** | 2.1 |
| `Package Reference`  | **Flunt** | 2.0.5 |

```
  4) INFRA
```

| Type    | Name  | Version |
| :-----:  | :---------: | :---: |
| `Target Framework`  | **net Standard** | 2.1 |
| `Package Reference`  | **EntityFrameworkCore** | 5.0.14 |
| `Package Reference`  | **Pomelo.EntityFrameworkCore.MySql** | 5.0.4 |

```
  5) TEST
```

| Type    | Name  | Version |
| :-----:  | :---------: | :---: |
| `Target Framework`  | **.net Core** | 5.0 |
| `Package Reference`  | **Xunit** | 2.4.1 |
| `Package Reference`  | **Moq** | 4.16.1 |
| `Package Reference`  | **Microsoft.NET.Test.Sdk** | 16.9.4 |

---

 **What would you improve if you had more time?**
> I would change performance about visualization data (exemple: pagination)

---

## FRONT-END (CLIENT)

```
  FRAMEWORKS
```

| Name  | Version |
| :---------: | :---: |
| **next** | 12.0.10 |
| **axios** | 0.25.0 |
| **antd design** | 4.18.6 |

**About project front-end (next.js)**
> It was easier for me to create a project in next.js because my everyday practice.
> But I don't rule out learning with vue.js. I learn fast
