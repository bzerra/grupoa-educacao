import Header from "./component/Header.js";
import styles from '../styles/Home.module.css'
import { Button } from "antd";
import Link from "next/link";

export default function Home() {
  return (
    <div className={styles.container}>

     <Header />

      <main className={styles.main}>
        
      <div style={{'text-align': '-webkit-center'}}>
        <h1 style={{color:"white"}} >Aplication Test</h1>
        <Link href={"/management/student"}>
        <Button type="primary">
        Access
        </Button>
        </Link>
      </div>

      </main>
      <footer className={styles.footer}>
        <p>
          Igor Marcelo Bezerra 
        </p>
      </footer>
    </div>
  )
}
