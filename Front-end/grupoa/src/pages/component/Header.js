import Head from 'next/head';

function Header(){
    return (
    <Head>
        <title>Group A System</title>
        <link rel="icon" href="/favicon.ico" />
    </Head>
    );
}


export default Header