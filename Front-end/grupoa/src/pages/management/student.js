import Header from "../component/Header"
import styles from '../../styles/Home.module.css'
import { Menu, Table, Tag, Space, Button, Drawer, Form, Input } from "antd"
import {RollbackOutlined, AuditOutlined} from '@ant-design/icons'
import Link from "next/link"
import { useEffect, useState } from "react"
import API from "../../service/apiService"
const { Search } = Input;

export default function Student(){
    

    const [visible, setVisible] = useState(false);
    const [idSelecionado, setIdSelecionado] = useState("");
    const [form] = Form.useForm();
    const [listStudent, setListStudent] = useState([]);
    const [loading, setLoading] = useState(true);
    const [Update, setUpdate] = useState(true);

    const showDrawer = async (id = "") =>
    {
      if(id !== '') {
        const {data: response} = await API.get('api/Student/'+id);
        form.setFieldsValue({name: response.return[0].name, cpf:response.return[0].cpf, ra: response.return[0].ra, email: response.return[0].email});
      }
        setIdSelecionado(id);
        setVisible(true);    
    };

    function onClose(){
      form.setFieldsValue({name: "", cpf:"", ra: "", email: ""});
        setVisible(false);
    };

    function handleChange(value) {
        // console.log(`selected ${value}`);
    }

    const onSearch = async value => {
      debugger;
      
      const {data: response} = await API.get('api/student?Name='+value);
      setListStudent(await response.return.map(x => (    
          {
              'key': x.id,
              'name': x.name,
              'cpf': x.cpf,
              'email': x.email,
              'ra': x.ra
          }
      )));
      console.log(value);
    };
    

    const onDeleteStudent = async (id) => {      
      var r = await API.delete('api/Student/'+id);
      onSearch("");
    };

    async function onFinish(values) {
        console.log('Success:', values);
        try {
          var form = new FormData();
          form.append("Name",values.name);
          form.append("Ra",values.ra);
          form.append("Email",values.email);
          form.append("Cpf",values.cpf);
          
    
          if(idSelecionado === ""){
            const {data: response} = await API.post('api/student', form);
          }else{
            form.append("id",idSelecionado);
            const {data: response} = await API.put('api/student/'+idSelecionado, form);
          }
    
          onSearch("");
          onClose();
        } catch (error) {
          console.error(error.message);
        }    
    };

    function onFinishFailed(errorInfo) {
        console.log('Failed:', errorInfo);
        console.log(form);
      };

    const columns = [
        {
            title: 'Ra',
            dataIndex: 'ra',
            key: 'ra',  
            sorter: (a, b) => a.ra - b.ra,           
        },
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',                   
        },
        {
          title: 'CPF',
          dataIndex: 'cpf',
          key: 'cpf',
          sorter: (a, b) => a.cpf - b.cpf,   
          render: text => <Tag color={'geekblue'} key={text}>{text.toUpperCase()}</Tag>,
        },        
        {
          title: 'Action',
          key: 'action',          
          render : (text, record) => (<div><Button type="primary" style={{margin:'5px'}} onClick={() => showDrawer(record.key)}>Update</Button><Button type="danger" onClick={() => onDeleteStudent(record.key)}>Delete</Button></div>)
        },
    ];
      

useEffect(() => {
  const fetchData = async () =>{
    setLoading(true);
    debugger;
    try {
      const {data: response} = await API.get('api/student');
      setListStudent(await response.return.map(x => (    
          {
              'key': x.id,
              'name': x.name,
              'cpf': x.cpf,
              'email': x.email,
              'ra': x.ra
          }
      )));
    } catch (error) {
      console.error(error.message);
    }
    setLoading(false);
  }

  fetchData();
}, []);

    return (
        <div className={styles.container}>
          
        <Menu mode="horizontal" theme="light"  >
        <Menu.Item key="student" icon={<AuditOutlined />}>
            <Link href={"/management/student"}>
                Student
            </Link>
        </Menu.Item>
        <Menu.Item key="home" icon={<RollbackOutlined />}>
            <Link href={"/"}>
                Back
            </Link>
        </Menu.Item>
        </Menu>
        <Header />

            <main  style={{padding:'20px'}}>
            <h1 style={{color:"white"}}>Student</h1>

            <div style={{'text-align': 'right'}}>
            <Space direction="vertical">
              <Search placeholder="search name" onSearch={onSearch} style={{ width: 200 }} />
            </Space>
            </div>

        <Table columns={columns} dataSource={listStudent} />

        <div>
            <Button type="primary" onClick= {() => showDrawer()}>Add Student</Button>
        </div>
        </main>

        <Drawer
          title="Student"
          placement={'right'}
          closable={false}
          onClose={() => onClose()}
          visible={visible}
          key={'student'}
        >
         <Form
              form={form}
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Name"
                name="name"
                rules={[
                  {
                    required: true,
                    message: 'important information',
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Ra"
                name="ra"
                rules={[
                  {
                    required: true,
                    message: 'important information',
                  },
                ]}        
              >
                  <Input />
              </Form.Item>

              <Form.Item
                label="CPF"
                name="cpf"
                disabled = {true}
                rules={[
                  {
                    required: true,
                    message: 'important information',
                  },
                ]}        
              >
                  <Input />
              </Form.Item>

              <Form.Item
                label="Email"
                name="email"
                rules={[
                  {
                    required: true,
                    message: 'important information',
                  },
                ]}        
              >
                  <Input />
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Button type="primary" htmlType="submit">
                  Finalize
                </Button>
              </Form.Item>
          </Form> 
        </Drawer>

        </div>
    );
}