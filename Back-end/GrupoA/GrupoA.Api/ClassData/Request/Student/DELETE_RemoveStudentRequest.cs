﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace GrupoA.Api.ClassData.Request.Student
{
    public class DELETE_RemoveStudentRequest
    {
        [Required, NotNull]
        public Guid Id { get; set; }
    }
}
