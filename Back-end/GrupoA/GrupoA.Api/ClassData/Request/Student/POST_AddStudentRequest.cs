﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace GrupoA.Api.ClassData.Request.Student
{
    public class POST_AddStudentRequest
    {
        [Required, NotNull]
        public string Name { get; set; }
        [Required, NotNull]
        public string Cpf { get; set; }
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required, NotNull]
        public string Ra { get; set; }
    }
}
