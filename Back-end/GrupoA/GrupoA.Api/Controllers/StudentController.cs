﻿using AutoMapper;
using GrupoA.Api.Base;
using GrupoA.Api.ClassData.Request.Student;
using GrupoA.Api.ClassData.Response.Student;
using GrupoA.Aplication.Commands.Student;
using GrupoA.Aplication.Querys;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrupoA.Api.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class StudentController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public StudentController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] POST_AddStudentRequest data)
        {
            var command = _mapper.Map<AddStudentCommand>(data);
            var result = await _mediator.Send(command, default);

            return Ok(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromForm] PUT_UpdateStudentRequest data)
        {
            var command = _mapper.Map<UpdateStudentCommand>(data);
            var result = await _mediator.Send(command, default);

            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var command = new RemoveStudentCommand(id.ToString());
            var result = await _mediator.Send(command, default);

            return Ok(result);
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get([FromRoute] string Id)
        {
            var command = new StudentQuery(Id);
            var result = await _mediator.Send(command, default);
            result.Return = new List<object> { result.Return.First() }.AsEnumerable();

            //return Ok(_mapper.Map<GET_Student>(result.Return.First()));
            return Ok(_mapper.Map<BaseResponseQuery<IEnumerable<GET_Student>>>(result));

        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GET_FilterStudentRequest data = null)
        {
            var command = _mapper.Map<StudentQuery>(data);
            var result = await _mediator.Send(command, default);

            return Ok(_mapper.Map<BaseResponseQuery<IEnumerable<GET_Student>>>(result));
        }

    }
}
