﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace GrupoA.Api.IoC.Swagger
{
    public static class SwaggerIoC
    {
        public static IServiceCollection ConfigureSwagger(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new OpenApiInfo {
                    Title = "GrupoA.API",
                    Version = "v1",
                    Description = $@"<div align='center'>
                                    <img src='{configuration.GetValue<string>("Swagger:ENVIRONMENT")}'/>
                                    <br />
                                    <br />
                                    Information about project - Group A
                                    </div>"

                });
            });

            return services;
        }

        public static IApplicationBuilder ConfigureSwaggerApp(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "GrupoA.API v1"));

            return app;
        }
    }
}
