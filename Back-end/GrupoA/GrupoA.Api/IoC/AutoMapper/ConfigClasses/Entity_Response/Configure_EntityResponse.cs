﻿using AutoMapper;
using GrupoA.Api.Base;
using GrupoA.Api.ClassData.Response.Student;
using GrupoA.Aplication.Base;
using GrupoA.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrupoA.Api.IoC.AutoMapper.ConfigClasses.Entity_Response
{
    public class Configure_EntityResponse : Profile
    {
        public Configure_EntityResponse()
        {
            
            //CreateMap<BaseReturnQuery<>, BaseResponseQuery>();
            CreateMap(typeof(BaseReturnQuery<>), typeof(BaseResponseQuery<>));

            #region Student
            CreateMap<Student, GET_Student>();
            #endregion
        }
    }
}
