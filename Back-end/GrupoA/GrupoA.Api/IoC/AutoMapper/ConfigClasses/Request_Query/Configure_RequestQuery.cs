﻿using AutoMapper;
using GrupoA.Api.ClassData.Request.Student;
using GrupoA.Aplication.Querys;

namespace GrupoA.Api.IoC.AutoMapper.ConfigClasses.Request_Query
{
    public class Configure_RequestQuery : Profile
    {
        public Configure_RequestQuery()
        {
            #region Student            
            CreateMap<GET_FilterStudentRequest, StudentQuery>()
                .ConstructUsing((src, ctx) => {

                    //var paginacao = ctx.Mapper.Map<Paginacao>(src.Paginacao);

                    return new StudentQuery(/*paginacao, src.Ordenacao, src.Campos*/)
                    .SetId(src.Id)
                    .SetName(src.Name)
                    .SetEmail(src.Email)
                    .SetCpf(src.Cpf)                    
                    .SetRa(src.Ra);
                })
                .IgnoreAllPropertiesWithAnInaccessibleSetter();
            #endregion
        }
    }
}
