﻿using AutoMapper;
using GrupoA.Api.ClassData.Request.Student;
using GrupoA.Aplication.Commands.Student;

namespace GrupoA.Api.IoC.AutoMapper.ConfigClasses.Request_Command
{
    public class Configure_RequestCommand : Profile
    {
        public Configure_RequestCommand()
        {
            #region Student
            CreateMap<POST_AddStudentRequest, AddStudentCommand>();
            CreateMap<PUT_UpdateStudentRequest, UpdateStudentCommand>();
            CreateMap<DELETE_RemoveStudentRequest, RemoveStudentCommand>(); 
            #endregion
        }
    }
}
