﻿using AutoMapper;
using GrupoA.Api.IoC.AutoMapper.ConfigClasses.Request_Command;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GrupoA.Api.IoC.AutoMapper
{
    public static class AutoMapperIoC
    {
        public static IServiceCollection ConfigureAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(AutoMapperIoC));

            var configAutoMapper = new MapperConfiguration(cfg => {

                cfg.AddProfile<Configure_RequestCommand>();

            }).CreateMapper();

            return services;
        }
    }
}
