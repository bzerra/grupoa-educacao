﻿using GrupoA.Api.Base.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrupoA.Api.Base
{
    
    public class BaseResponseQuery
    {
        public bool Sucesso { get; set; }
        public Dictionary<string, string[]> Errors { get; set; }
    }

    public class BaseResponseQuery<TypeReturn> : BaseResponseQuery
    {        
        public TypeReturn Return { get; set; }
        public int Page { get; set; }
    }
}
