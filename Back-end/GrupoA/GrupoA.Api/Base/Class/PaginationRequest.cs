﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrupoA.Api.Base.Class
{
    public class PaginationRequest
    {
        public int Page { get; set; } = 1;
        public int TotalPage { get; set; } = 15;
    }
}
