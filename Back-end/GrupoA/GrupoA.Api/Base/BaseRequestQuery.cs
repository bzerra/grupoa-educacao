﻿using GrupoA.Api.Base.Class;
using System.Collections.Generic;

namespace GrupoA.Api.Base
{
    public class BaseRequestQuery
    {
        public string Ordination { get; set; }

        public PaginationRequest Pagination { get; set; }

        public IEnumerable<string> Fields { get; set; }
    }
   
}
