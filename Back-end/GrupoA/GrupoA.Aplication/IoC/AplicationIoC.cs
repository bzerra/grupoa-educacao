﻿using GrupoA.Domain.Contract.Service;
using GrupoA.Domain.Service;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Aplication.IoC
{
    public static class AplicationIoC
    {
        public static IServiceCollection ConfigureAplication(this IServiceCollection services)
        {
            #region MediaRt
            services.AddMediatR(typeof(AplicationIoC));
            #endregion

            #region Services
            services.AddScoped<IStudentService, StudentService>();
            #endregion

            return services;
        }
    }
}
