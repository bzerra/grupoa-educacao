﻿using GrupoA.Aplication.Base;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GrupoA.Aplication.Events.Student
{

    public class StudentHandler : IRequestHandler<AddedStudentEvent, bool>
    {
        public Task<bool> Handle(AddedStudentEvent request, CancellationToken cancellationToken)
        {
            Console.WriteLine($"Event executed: {typeof(AddedStudentEvent)}");

            return Task.FromResult(true);
        }
    }
}
