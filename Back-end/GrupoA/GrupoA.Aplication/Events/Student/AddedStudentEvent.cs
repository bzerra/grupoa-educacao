﻿using GrupoA.Aplication.Base;
using GrupoA.Aplication.Commands.Student;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Aplication.Events.Student
{
    public class AddedStudentEvent : BaseEvent<AddStudentCommand>
    {
        public AddedStudentEvent(Guid id, AddStudentCommand command) : base(command)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
