﻿using Flunt.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Aplication.Base
{
    public abstract class BaseCommand<TypeReturn> : Notifiable<Notification>, IRequest<BaseReturnCommand<TypeReturn>>
    {

    }
}
