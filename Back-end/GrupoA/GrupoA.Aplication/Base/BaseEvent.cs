﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Aplication.Base
{
    public abstract class BaseEvent<TypeCommand> : IRequest<bool>
    {
        protected BaseEvent(TypeCommand command)
        {
            _command = command;
        }
        public TypeCommand _command { get; set; }
    }
}
