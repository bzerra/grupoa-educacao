﻿using Flunt.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Aplication.Base
{
    public class BaseReturnCommand<TypeReturn>
    {
        public BaseReturnCommand(IEnumerable<Notification> erros)
        {
            this.erros = erros;
            Completed = false;
        }

        public BaseReturnCommand(TypeReturn result)
        {
            Return = result;
            Completed = true;
        }

        public bool Completed { get; set; }
        public TypeReturn Return { get; set; }
        public IEnumerable<Notification> erros { get; set; }
    }
}
