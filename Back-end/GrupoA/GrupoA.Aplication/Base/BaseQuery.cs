﻿using GrupoA.Domain.Base.Interface;
using MediatR;
using System.Collections.Generic;

namespace GrupoA.Aplication.Base
{
    public abstract class BaseQuery<Entity, TypeSpecification, TypeReturn> : IRequest<BaseReturnQuery<IEnumerable<TypeReturn>>>
    {
        protected BaseQuery()
        {
        }

        public abstract ISpecification<Entity> ToSpecification();

        public abstract ISpecification<Entity> ToSpecification(TypeSpecification spec);

    }
}
