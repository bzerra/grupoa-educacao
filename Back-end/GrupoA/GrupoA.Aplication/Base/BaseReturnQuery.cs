﻿using MediatR;

namespace GrupoA.Aplication.Base
{
    public class BaseReturnQuery<TypeReturn>
    {
        public BaseReturnQuery(TypeReturn returnObj)
        {
            Return = returnObj;
            Page = 0;
        }

        public TypeReturn Return { get; set; }
        public int Page { get; set; }
    }
}
