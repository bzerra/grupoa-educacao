﻿using GrupoA.Aplication.Base;
using GrupoA.Domain.Base.Interface;
using GrupoA.Domain.Entity;
using GrupoA.Domain.Specification;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Aplication.Querys
{
    public class StudentQuery : BaseQuery<Student, StudentSpecification, object>
    {


        public Guid? Id { get; private set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Cpf { get; private set; }
        public string Ra { get; private set; }

        public StudentQuery()
        {
        }

        public StudentQuery SetId(Guid? id)
        {
            this.Id = Id;
            return this;
        }

        public StudentQuery SetName(string name)
        {
            this.Name = name;
            return this;
        }

        public StudentQuery SetEmail(string email)
        {
            this.Email = email;
            return this;
        }

        public StudentQuery SetCpf(string cpf)
        {
            this.Cpf = cpf;
            return this;
        }

        public StudentQuery SetRa(string ra)
        {
            this.Ra = ra;
            return this;
        }

        public StudentQuery(string id)
        {
            if(!Guid.TryParse(id, out Guid idResult)) {

            }
            Id = idResult;
        }

        public override ISpecification<Student> ToSpecification()
        {
            return ToSpecification(new StudentSpecification());
        }

        public override ISpecification<Student> ToSpecification(StudentSpecification spec)
        {
            if (Id.HasValue)
                spec.FilterForId(Id.Value);

            if (string.IsNullOrWhiteSpace(Name) is false)
                spec.FilterForName(Name);

            if (string.IsNullOrWhiteSpace(Email) is false)
                spec.FilterForEmail(Email);

            if (string.IsNullOrWhiteSpace(Cpf) is false)
                spec.FilterForCpf(Cpf);

            if (string.IsNullOrWhiteSpace(Ra) is false)
                spec.FilterForRa(Ra);

            return spec;
        }
    }
}
