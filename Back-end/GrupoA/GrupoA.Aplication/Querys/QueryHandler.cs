﻿using GrupoA.Aplication.Base;
using GrupoA.Domain.Base.Interface;
using GrupoA.Domain.Entity;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GrupoA.Aplication.Querys
{
    public class QueryHandler : IRequestHandler<StudentQuery, BaseReturnQuery<IEnumerable<object>>>
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _unitOfWork;

        public QueryHandler(IMediator mediator, IUnitOfWork unitOfWork)
        {
            _mediator = mediator;
            _unitOfWork = unitOfWork;
        }

        public async Task<BaseReturnQuery<IEnumerable<object>>> Handle(StudentQuery request, CancellationToken cancellationToken)
        {
           var spec = request.ToSpecification();

            var repo = _unitOfWork.Repository<Student>();
            var result = await repo.GetEntity(spec);

            return new BaseReturnQuery<IEnumerable<object>>(result);
        }
    }
}
