﻿using GrupoA.Aplication.Base;
using GrupoA.Aplication.Events.Student;
using GrupoA.Domain.Contract.Service;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GrupoA.Aplication.Commands.Student
{
    public class StudentHandler : 
        IRequestHandler<AddStudentCommand, BaseReturnCommand<Guid>>,
        IRequestHandler<UpdateStudentCommand, BaseReturnCommand<Guid>>,
        IRequestHandler<RemoveStudentCommand, BaseReturnCommand<bool>>
    {

        private readonly IMediator _mediator;
        private readonly IStudentService _service;

        public StudentHandler(IMediator mediator, IStudentService service)
        {
            _mediator = mediator;
            _service = service;
        }

        public async Task<BaseReturnCommand<Guid>> Handle(AddStudentCommand request, CancellationToken cancellationToken)
        {
            if (request.IsValid is false)
                return new BaseReturnCommand<Guid>(request.Notifications);

            var student = await _service.AddStudent(request.Name, request.Cpf, request.Email, request.Ra);

            if(_service.IsValid is false)
                return new BaseReturnCommand<Guid>(_service.Notifications);

            //Event if necessary
            await _mediator.Send(new AddedStudentEvent(student.Id, request), default);

            return new BaseReturnCommand<Guid>(student.Id);
        }

        public async Task<BaseReturnCommand<Guid>> Handle(UpdateStudentCommand request, CancellationToken cancellationToken)
        {
            if (request.IsValid is false)
                return new BaseReturnCommand<Guid>(request.Notifications);

            var student = await _service.UpdateStudent(request.Id ,request.Name, request.Cpf, request.Email, request.Ra);

            if (_service.IsValid is false)
                return new BaseReturnCommand<Guid>(_service.Notifications);

            //Event if necessary
            //_mediator.Send();

            return new BaseReturnCommand<Guid>(student.Id);
        }

        public async Task<BaseReturnCommand<bool>> Handle(RemoveStudentCommand request, CancellationToken cancellationToken)
        {
            if (request.IsValid is false)
                return new BaseReturnCommand<bool>(request.Notifications);

            var student = await _service.DeleteStudent(request.Id);

            if (_service.IsValid is false)
                return new BaseReturnCommand<bool>(_service.Notifications);

            //Event if necessary
            //_mediator.Send();

            return new BaseReturnCommand<bool>(true);
        }
    }
}
