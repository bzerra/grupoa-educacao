﻿using GrupoA.Aplication.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Aplication.Commands.Student
{
    public class RemoveStudentCommand : BaseCommand<bool>
    {
        public RemoveStudentCommand(string id)
        {
            Id = Guid.Parse(id);
        }

        public Guid Id { get; set; }
    }
}
