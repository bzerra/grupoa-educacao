﻿using GrupoA.Aplication.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Aplication.Commands.Student
{
    public class AddStudentCommand : BaseCommand<Guid>
    {
        public string Name { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Ra { get; set; }
    }
}
