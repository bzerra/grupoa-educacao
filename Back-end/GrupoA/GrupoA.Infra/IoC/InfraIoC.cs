﻿using GrupoA.Infra.EF.Ioc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GrupoA.Infra.IoC
{
    public static class InfraIoC
    {
        public enum ETypeStorage
        {
            InMemory = 1,
            MySql = 2,
            Postgress = 3
        }


        public static IServiceCollection ConfigureInfra(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureEF(configuration, ETypeStorage.MySql);
            return services;
        }
    }
}
