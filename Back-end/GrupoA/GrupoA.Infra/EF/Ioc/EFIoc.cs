﻿using GrupoA.Domain.Base.Interface;
using GrupoA.Infra.Base.EF;
using GrupoA.Infra.EF.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using static GrupoA.Infra.IoC.InfraIoC;

namespace GrupoA.Infra.EF.Ioc
{
    public static class EFIoc
    {
        public static IServiceCollection ConfigureEF(this IServiceCollection services, IConfiguration configuration, ETypeStorage typeStorage)
        {
            services.AddDbContext<GroupAContext>(opts => {

                if (typeStorage.Equals(ETypeStorage.InMemory)) 
                {
                    opts.UseInMemoryDatabase("GroupA")/*.UseInternalServiceProvider()*/;
                }
                else 
                {
                    var connString = configuration.GetConnectionString("GroupAContext");
                    var mysqlVersion = ServerVersion.AutoDetect(configuration.GetConnectionString("GroupAContext")); //new MySqlServerVersion(new Version(8, 0, 21)),

                    opts.UseMySql(connString, mysqlVersion, mySqlOptions => {
                        mySqlOptions.CommandTimeout(360);
                        mySqlOptions.EnableIndexOptimizedBooleanColumns();
                        mySqlOptions.EnableRetryOnFailure(5);
                        mySqlOptions.UseRelationalNulls();
                        mySqlOptions.MigrationsAssembly("GrupoA.Infra");
                    });
                    opts.EnableSensitiveDataLogging();
                }

            });

            services.AddScoped<IUnitOfWork, EntityFrameworkUnitOfWork<GroupAContext>>();

            return services;
        }
    }
}
