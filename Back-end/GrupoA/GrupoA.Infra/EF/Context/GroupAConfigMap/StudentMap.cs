﻿using GrupoA.Domain.Entity;
using GrupoA.Infra.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Infra.EF.Context.GroupAConfigMap
{
    public class StudentMap : BaseEFConfigMap<Student>, IEntityTypeConfiguration<Student>
    {
        public override void Configure(EntityTypeBuilder<Student> builder)
        {
            base.Configure(builder);

            builder.ToTable("Student");

            builder.Property(x => x.Name).HasColumnName("Name").IsRequired();
            builder.Property(x => x.Cpf).HasColumnName("Cpf").IsRequired();
            builder.Property(x => x.Email).HasColumnName("Email").IsRequired();
            builder.Property(x => x.Ra).HasColumnName("Ra").IsRequired();
        }
    }
}
