﻿using Flunt.Notifications;
using GrupoA.Domain.Base;
using GrupoA.Infra.EF.Context.GroupAConfigMap;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrupoA.Infra.EF.Context
{
    public class GroupAContext : DbContext
    {
        public GroupAContext(DbContextOptions<GroupAContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfigurationsFromAssembly(
                typeof(GroupAContext).Assembly, type => {
                var caminhoModelosConfig = string.Join('.', typeof(StudentMap).FullName?.Split('.').SkipLast(1));
                return type.FullName?.StartsWith(caminhoModelosConfig) ?? false;
            });

            modelBuilder.Ignore<Notification>();
            modelBuilder.Ignore<Notifiable<Notification>>();
            modelBuilder.Ignore<BaseEntity>();

            base.OnModelCreating(modelBuilder);
        }
    }
}

//packge manager console
// Add-Migration MyFirstMigration -context GrupoA.Infra.EF.Context.GroupAContext -output EF/Migrations/GroupAContext_Migration
// Update-Database -context GrupoA.Infra.EF.Context.GroupAContext
