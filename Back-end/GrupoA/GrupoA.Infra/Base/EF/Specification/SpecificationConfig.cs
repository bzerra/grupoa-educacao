﻿using GrupoA.Domain.Base;
using GrupoA.Domain.Base.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrupoA.Infra.Base.EF.Specification
{
    public static class SpecificationConfig<Entity> where Entity : BaseEntity
    {
        public static IQueryable<Entity> GetQuery(IQueryable<Entity> inputQuery, ISpecification<Entity> specification, bool paginar = true)
        {
            var query = inputQuery;

            #region Conditions query
            if (specification.Criteria is not null) {
                query = query.Where(specification.Criteria);
            }
            #endregion

            #region Includes query
            if (specification.Includes.Any()) {
                query = specification.Includes.Aggregate(query, (current, include)
                    => current.Include(include));
            }

            if (specification.IncludeStrings.Any()) {
                query = specification.IncludeStrings.Aggregate(query, (current, include)
                    => current.Include(include));
            }
            #endregion

            #region OrderBy
            if (specification.OrderBy is not null) {
                query = query.OrderBy(specification.OrderBy);
            }
            else if (specification.OrderByDescending is not null) {
                query = query.OrderByDescending(specification.OrderByDescending);
            }
            #endregion

            #region GroupBy XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

            #endregion

            #region Pagination
            if (paginar && specification.IsPaginacaoHabilitada) {
                query = query.Skip(specification.Skip).Take(specification.Take);
            }
            #endregion

            return query;
        }

        public static IQueryable<TResultado> GetQueryObject<TResultado>(IQueryable<Entity> inputQuery, ISpecification<Entity> specification, bool paginar = true)
        {
            var query = GetQuery(inputQuery, specification, paginar);

            return specification?.Select is null
                    ? query.Cast<TResultado>()
                    : query.Select(specification.Select).Cast<TResultado>();
        }
    }
}
