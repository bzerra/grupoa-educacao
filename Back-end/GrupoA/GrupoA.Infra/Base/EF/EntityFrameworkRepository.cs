﻿using GrupoA.Domain.Base;
using GrupoA.Domain.Base.Interface;
using GrupoA.Infra.Base.EF.Specification;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrupoA.Infra.Base.EF
{
    public class EntityFrameworkRepository<Entity> : IRepository<Entity> where Entity : BaseEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<Entity> _dbSet;

        public EntityFrameworkRepository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<Entity>();
        }

        public Task<Entity> AddEntity(Entity obj)
        {
            return Task.FromResult(_dbSet.Add(obj).Entity);
        }

        public Task<IEnumerable<Entity>> AddEntity(IEnumerable<Entity> listObj)
        {
            _dbSet.AddRangeAsync(listObj);
            return Task.FromResult(listObj);
        }

        public Task<bool> ContainsAsync(ISpecification<Entity> spec = null)
        {
            var resultQuery = SpecificationConfig<Entity>.GetQuery(_dbSet.AsQueryable(), spec, true);
            return Task.FromResult<bool>(resultQuery.Any());
        }

        public Task<bool> ContainsAsync(Func<Entity, bool> func)
        {
            return Task.FromResult(_dbSet.Where(func).Any());
        }

        public Task<long> CountAsync(ISpecification<Entity> spec = null)
        {
            var resultQuery = SpecificationConfig<Entity>.GetQuery(_dbSet.AsQueryable(), spec, true);
            return Task.FromResult<long>(resultQuery.LongCount());
        }

        public Task<bool> DeleteEntity(Entity obj)
        {
            _dbSet.Remove(obj);
            return Task.FromResult(true);
        }

        public Task<bool> DeleteEntity(IEnumerable<Entity> listObj)
        {
            _dbSet.RemoveRange(listObj);
            return Task.FromResult(true);
        }

        public Task<IEnumerable<Entity>> GetEntity(ISpecification<Entity> spec = null)
        {
            var resultQuery = SpecificationConfig<Entity>.GetQuery(_dbSet.AsQueryable(), spec, true);
            return Task.FromResult<IEnumerable<Entity>>(resultQuery);
        }

        public Task<Entity> GetEntityById(Guid id)
        {
            return Task.FromResult(_dbSet.Find(id));
        }

        public Task<Entity> UpdateEntity(Entity obj)
        {
            return Task.FromResult(_dbSet.Update(obj).Entity);
        }
    }
}
