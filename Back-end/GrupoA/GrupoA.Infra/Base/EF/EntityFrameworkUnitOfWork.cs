﻿using GrupoA.Domain.Base;
using GrupoA.Domain.Base.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace GrupoA.Infra.Base.EF
{
    public class EntityFrameworkUnitOfWork<TypeContext> : IUnitOfWork, IDisposable 
        where TypeContext : DbContext
    {
        private readonly TypeContext _context;
        private readonly Hashtable _repositories;

        public EntityFrameworkUnitOfWork(TypeContext context)
        {
            _context = context;
            _repositories = new Hashtable();
        }

        public int Commit()
        {
            return _context.SaveChangesAsync().Result;
        }

        public void Dispose()
        {
        }

        public bool Ping()
        {
            var tokenSource = new CancellationTokenSource(1000);
            var result = _context.Database.CanConnectAsync(tokenSource.Token).Result;
            return result;
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : BaseEntity
        {
            var nameTypeEntity = typeof(TEntity).Name;

            if (!_repositories.ContainsKey(nameTypeEntity)) 
            {
                var typeRepository = typeof(EntityFrameworkRepository<>);
                var repository = Activator
                    .CreateInstance(typeRepository
                    .MakeGenericType(typeof(TEntity)), _context);

                _repositories.Add(nameTypeEntity, repository);
            }

            return (IRepository<TEntity>)_repositories[nameTypeEntity];
        }
    }
}
