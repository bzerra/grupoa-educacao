﻿using GrupoA.Domain.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Infra.Base
{
    public abstract class BaseEFConfigMap<Entity> where Entity : BaseEntity
    {
        public virtual void Configure(EntityTypeBuilder<Entity> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(e => e.Id).IsRequired();

            #region Ignore
            builder.Ignore(x => x.Notifications);
            builder.Ignore(x => x.Updated);
            builder.Ignore(x => x.IsValid);
            #endregion

            #region Config Register
            builder.Property<DateTime>("DataCreate").ValueGeneratedOnAdd();
            builder.Property<DateTime>("DataUpdate").ValueGeneratedOnAddOrUpdate();

            builder.Property<DateTime>("DataCreate").Metadata.SetBeforeSaveBehavior(PropertySaveBehavior.Ignore);
            builder.Property<DateTime>("DataCreate").Metadata.SetAfterSaveBehavior(PropertySaveBehavior.Ignore);
            builder.Property<DateTime>("DataUpdate").Metadata.SetBeforeSaveBehavior(PropertySaveBehavior.Ignore);
            builder.Property<DateTime>("DataUpdate").Metadata.SetAfterSaveBehavior(PropertySaveBehavior.Ignore); 
            #endregion

        }

        
    }
}
