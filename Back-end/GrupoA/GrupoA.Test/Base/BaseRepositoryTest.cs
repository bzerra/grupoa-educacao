﻿using GrupoA.Domain.Base;
using GrupoA.Domain.Base.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrupoA.Test.Base
{
    class BaseRepositoryTest<Entity> : IRepository<Entity> where Entity : BaseEntity
    {
        public List<Entity> ListEntity;

        public BaseRepositoryTest(List<Entity> list = null)
        {
            if (list is null)
                ListEntity = new List<Entity>();
            else
                ListEntity = list;
        }

        public Task<Entity> AddEntity(Entity obj)
        {
            ListEntity.Add(obj);
            return Task.FromResult(obj);
        }

        public Task<IEnumerable<Entity>> AddEntity(IEnumerable<Entity> listObj)
        {
            ListEntity.AddRange(listObj);
            return Task.FromResult(listObj);
        }

        public Task<bool> ContainsAsync(ISpecification<Entity> spec = null)
        {
            return Task.FromResult(ListEntity.Where(spec.Criteria.Compile()).Any());
        }

        public Task<bool> ContainsAsync(Func<Entity, bool> func)
        {
            return Task.FromResult(ListEntity.Where(func).Any());
        }

        public Task<long> CountAsync(ISpecification<Entity> spec = null)
        {
            return Task.FromResult(ListEntity.Where(spec.Criteria.Compile()).LongCount());
        }

        public Task<bool> DeleteEntity(Entity obj)
        {
            ListEntity.Remove(obj);        
            return Task.FromResult(true);
        }

        public Task<bool> DeleteEntity(IEnumerable<Entity> listObj)
        {
            foreach (var item in listObj) {
                ListEntity.Remove(item);
            }
            return Task.FromResult(true);
        }

        public Task<IEnumerable<Entity>> GetEntity(ISpecification<Entity> spec = null)
        {
            return Task.FromResult(ListEntity.Where(spec.Criteria.Compile()));
        }

        public Task<Entity> GetEntityById(Guid id)
        {
            return Task.FromResult(ListEntity.Where(x => x.Id.Equals(id)).SingleOrDefault());
        }

        public Task<Entity> UpdateEntity(Entity obj)
        {
            var objAntigo = ListEntity.Where(x => x.Id.Equals(obj.Id)).SingleOrDefault();
            if (objAntigo is not null) ListEntity.Remove(objAntigo);
            ListEntity.Add(obj);
            return Task.FromResult(obj);
        }
    }
}
