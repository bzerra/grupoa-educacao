﻿using GrupoA.Domain.Entity;
using GrupoA.Domain.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GrupoA.Test._01___Domain.Specification
{
    public class StudentSpecificationTest
    {
        public List<Student> ListStudent;

        public StudentSpecificationTest()
        {
            ListStudent = new List<Student>() {
                new Student("Igor Bezerra", "55379389287", "igor@teste.com", "12345"),
                new Student("Flavio Rocha", "41138764280", "flavio@teste.com", "54321"),
                new Student("José", "37373095909", "jose@teste.com", "258963"),
            };

        }

        [Fact]
        public void FilterName()
        {
            var name = "Igor";
            var spec = new StudentSpecification().FilterForName(name);

            var student = ListStudent.Where(spec.Criteria.Compile()).SingleOrDefault();

            Assert.Contains(ListStudent, student => student.Name.Contains(name));
        }

        [Fact]
        public void FilterEmail()
        {
            var email = "flavio@teste.com";
            var spec = new StudentSpecification().FilterForEmail(email);

            var student = ListStudent.Where(spec.Criteria.Compile()).SingleOrDefault();

            Assert.Contains(ListStudent, student => student.Email.Contains(email));
        }
    }
}
