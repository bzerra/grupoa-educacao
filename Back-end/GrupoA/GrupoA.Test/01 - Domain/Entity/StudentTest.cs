﻿using GrupoA.Domain.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace GrupoA.Test._01___Domain.Entity
{
    public class StudentTest
    {
        #region NAME
        [Fact]
        public void InsetName_Valid()
        {
            var student = new Student();
            student.UpdateName("Igor", true);
            Assert.True(student.IsValid);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        public void InsetName_NotValid(string name)
        {
            var student = new Student();
            student.UpdateName(name, true);

            Assert.True(student.IsValid is false);
            Assert.True(student.Notifications.Any());
        }
        #endregion

        #region CPF
        [Fact]
        public void InsetCpf_Valid()
        {
            var student = new Student();
            student.UpdateCpf("55379389287", true);
            Assert.True(student.IsValid);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        [InlineData("553793892875")]
        public void InsetCpf_NotValid(string cpf)
        {
            var student = new Student();
            student.UpdateCpf(cpf, true);

            Assert.True(student.IsValid is false);
            Assert.True(student.Notifications.Any());
        }
        #endregion

        #region EMAIL
        [Fact]
        public void InsetEmail_Valid()
        {
            var student = new Student();
            student.UpdateEmail("infra@teste.com", true);
            Assert.True(student.IsValid);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        public void InsetEmail_NotValid(string email)
        {
            var student = new Student();
            student.UpdateEmail(email, true);

            Assert.True(student.IsValid is false);
            Assert.True(student.Notifications.Any());
        }
        #endregion
    }
}
