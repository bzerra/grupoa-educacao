﻿using GrupoA.Domain.Base.Interface;
using GrupoA.Domain.Contract.Service;
using GrupoA.Domain.Entity;
using GrupoA.Domain.Service;
using GrupoA.Test.Base;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace GrupoA.Test._01___Domain.Service
{
    public class StudentServiceTest
    {
        #region ADD

        [Fact]
        public async Task AddStudent_DataValid_Success()
        {
            var unitOfWork_Mock = new Mock<IUnitOfWork>();
            unitOfWork_Mock.Setup(x => x.Repository<Student>()).Returns(new BaseRepositoryTest<Student>());

            var service = new StudentService(unitOfWork_Mock.Object);

            var student01 = await service.AddStudent("Igor Bezerra", "55379389287", "igor@teste.com", "1235648");
            
            Assert.NotNull(student01);
            Assert.True(student01.IsValid);
            Assert.True(service.IsValid);
        }

        [Fact]
        public async Task AddStudent_DataNotValid_ErroEntity_Fail()
        {
            var unitOfWork_Mock = new Mock<IUnitOfWork>();
            unitOfWork_Mock.Setup(x => x.Repository<Student>()).Returns(new BaseRepositoryTest<Student>());

            var service = new StudentService(unitOfWork_Mock.Object);

            var student01 = await service.AddStudent("Igor Bezerra", "", "igor@teste.com", "1235648");

            Assert.Null(student01);            
            Assert.True(service.IsValid is false);
            Assert.True(service.Notifications.Any());
        }

        [Fact]
        public async Task AddStudent_DataNotValid_ErroValidation_Fail()
        {
            var unitOfWork_Mock = new Mock<IUnitOfWork>();
            unitOfWork_Mock.Setup(x => x.Repository<Student>()).Returns(new BaseRepositoryTest<Student>());

            var service = new StudentService(unitOfWork_Mock.Object);

            var student01 = await service.AddStudent("Igor Bezerra", "55379389287", "igor@teste.com", "1235648");

            var student02 = await service.AddStudent("Igor Bezerra", "55379389287", "igor@teste.com", "1235648");

            Assert.Null(student02);
            Assert.True(service.IsValid is false);
            Assert.True(service.Notifications.Any());
        }

        [Fact]
        public async Task AddStudent_DataValid_ErroCommit_Fail()
        {
            var unitOfWork_Mock = new Mock<IUnitOfWork>();
            unitOfWork_Mock.Setup(x => x.Repository<Student>()).Returns(new BaseRepositoryTest<Student>());
            unitOfWork_Mock.Setup(x => x.Commit()).Returns(-1);

            var service = new StudentService(unitOfWork_Mock.Object);

            var student01 = await service.AddStudent("Igor Bezerra", "55379389287", "igor@teste.com", "1235648");

            Assert.Null(student01);
            Assert.True(service.IsValid is false);
            Assert.True(service.Notifications.Any());
        }

        #endregion
    }
}
