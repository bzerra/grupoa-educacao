﻿using Flunt.Notifications;
using GrupoA.Aplication.Commands.Student;
using GrupoA.Domain.Contract.Service;
using GrupoA.Domain.Entity;
using MediatR;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GrupoA.Test._02___Aplication.Command
{
    public class StudentHandlerTest
    {
        private readonly Mock<IStudentService> _serviceStudentMock;
        private readonly Mock<IMediator> _imediatorMock;
        private readonly List<Notification> _notifications = new List<Notification>();

        public StudentHandlerTest()
        {
            _serviceStudentMock = new Mock<IStudentService>();
            _imediatorMock = new Mock<IMediator>();

            _serviceStudentMock.Setup(x => x.Notifications).Returns(_notifications);

            _serviceStudentMock.Setup(x => x.AddStudent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns<string, string, string, string>((name, cpf, email, ra)  =>  {
                    return Task.FromResult(new Student(name, cpf, email, ra));
                });
        }

        #region ADD
        [Fact]
        public async Task AddStudentCommand_DataValid_Success()
        {
            var command = new AddStudentCommand() {
                Name = "Igor",
                Email = "igor@test.com",
                Cpf = "37373095909",
                Ra = "123456"
            };

            _serviceStudentMock.Setup(x => x.IsValid).Returns(true);
            var handler = new StudentHandler(_imediatorMock.Object, _serviceStudentMock.Object);

            var result = await handler.Handle(command, default);
            Assert.True(result.Completed);
        }

        [Fact]
        public async Task AddStudentCommand_DataValid_Fail_ErroService()
        {
            var command = new AddStudentCommand() {
                Name = "Igor",
                Email = "igor@test.com",
                Cpf = "37373095909",
                Ra = "123456"
            };

            _serviceStudentMock.Setup(x => x.IsValid).Returns(false);
            _notifications.Add(new Notification("Erro", "script"));

            var handler = new StudentHandler(_imediatorMock.Object, _serviceStudentMock.Object);

            var result = await handler.Handle(command, default);
            Assert.True(result.erros.Any());
        }
        #endregion
    }
}
