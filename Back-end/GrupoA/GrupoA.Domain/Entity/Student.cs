﻿using Flunt.Notifications;
using Flunt.Validations;
using GrupoA.Domain.Base;

namespace GrupoA.Domain.Entity
{
    public class Student : BaseEntity
    {

        public Student()
        {
        }

        public Student(string name, string cpf, string email, string ra)
        {
            UpdateName(name, newObj: true);
            UpdateCpf(cpf, newObj: true);
            UpdateEmail(email, newObj: true);
            UpdateRa(ra, newObj: true);

            Updated = false;
        }

        #region Functions
        public Student UpdateRa(string ra, bool newObj = false)
        {
            if (newObj is false && Ra.Equals(ra))
                return this;

            Ra = ra;
            AddNotifications(
                new Contract<Notification>()
                .IsNotNullOrWhiteSpace(Ra, "Ra")
                .Requires()
            );

            return this;
        }

        public Student UpdateEmail(string email, bool newObj = false)
        {
            if (newObj is false && Email.Equals(email))
                return this;

            Email = email;
            AddNotifications(
                new Contract<Notification>()
                .IsNotNullOrWhiteSpace(Email, "Email")
                .Requires()
            );

            return this;
        }

        public Student UpdateCpf(string cpf, bool newObj = false)
        {
            if (newObj is false && Cpf.Equals(cpf))
                return this;

            Cpf = cpf;
            AddNotifications(
                new Contract<Notification>()
                .IsNotNullOrWhiteSpace(Cpf, "Cpf")
                .IsTrue(Cpf is not null ?  Cpf.Length.Equals(11) : false, "Cpf")
                .Requires()
            );

            return this;
        }

        public Student UpdateName(string name, bool newObj = false)
        {
            if (newObj is false && Name.Equals(name))
                return this;

            Name = name;
            AddNotifications(
                new Contract<Notification>()
                .IsNotNullOrWhiteSpace(Name, "Name")
                .Requires()
            );

            return this;
        } 
        #endregion

        public string Name { get; private set; }
        public string Cpf { get; private set; }
        public string Email { get; private set; }
        public string Ra { get; private set; }
    }
}
