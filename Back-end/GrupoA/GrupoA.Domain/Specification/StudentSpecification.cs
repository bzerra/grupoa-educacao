﻿using GrupoA.Domain.Base;
using GrupoA.Domain.Base.Interface;
using GrupoA.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Domain.Specification
{
    public class StudentSpecification : BaseSpecification<Student, StudentSpecification>
    {
        public StudentSpecification()
        {
        }

        public virtual ISpecification<Student> FilterForName(string name)
        {
            AddCriteria(x => x.Name.Contains(name));
            return this;
        }

        public virtual ISpecification<Student> FilterForCpf(string cpf)
        {
            AddCriteria(x => x.Cpf.Contains(cpf));
            return this;
        }

        public virtual ISpecification<Student> FilterForEmail(string email)
        {
            AddCriteria(x => x.Email.Contains(email));
            return this;
        }

        public virtual ISpecification<Student> FilterForRa(string ra)
        {
            AddCriteria(x => x.Ra.Contains(ra));
            return this;
        }
    }
}
