﻿// Bezerrus Softwares
// Projeto com a implementação de Multimídia Ibrem
// Copyright (c) 2021, Igor Marcelo Bezerra

using GrupoA.Domain.Base.Interface;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace GrupoA.Domain.Base
{
    public abstract class BaseSpecification<TEntity, ISpecification> : ISpecification<TEntity>
        where ISpecification : BaseSpecification<TEntity, ISpecification>
        where TEntity : BaseEntity
    {
        protected BaseSpecification()
        {
        }

        #region Atributes

        public List<string> IncludeStrings { get; private set; } = new();

        public Expression<Func<TEntity, bool>> Criteria { get; private set; }

        public List<Expression<Func<TEntity, object>>> Includes { get; } = new List<Expression<Func<TEntity, object>>>();

        public Expression<Func<TEntity, object>> OrderBy { get; private set; }

        public Expression<Func<TEntity, object>> OrderByDescending { get; private set; }

        public Expression<Func<TEntity, object>> GroupBy { get; private set; }

        public Expression<Func<TEntity, object>> Select { get; protected set; }

        public bool IsCamposPersonalizados => Select != null;

        public int Take { get; private set; }

        public int Skip { get; private set; }

        public bool IsPaginacaoHabilitada { get; private set; } = false;

        #endregion

        #region Metodos 

        protected virtual ISpecification<TEntity> AddInclude(Expression<Func<TEntity, object>> include)
        {
            Includes.Add(include);
            return this;
        }

        public virtual ISpecification<TEntity> AddCriteria(Expression<Func<TEntity, bool>> criteria)
        {
            Criteria = criteria;
            return this;
        }

        #endregion

        #region Specifications

        public virtual ISpecification<TEntity> FilterForId(Guid Id)
        {
            AddCriteria(x => x.Id.Equals(Id));
            return this;
        }

        #endregion
    }
}
