﻿using GrupoA.Domain.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoA.Domain.Base.Interface
{
    public interface IUnitOfWork
    {
        int Commit();
        bool Ping();
        IRepository<TEntity> Repository<TEntity>() where TEntity : BaseEntity;
    }
}
