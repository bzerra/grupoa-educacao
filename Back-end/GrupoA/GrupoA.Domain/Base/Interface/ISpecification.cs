﻿// Bezerrus Softwares
// Projeto com a implementação de Multimídia Ibrem
// Copyright (c) 2021, Igor Marcelo Bezerra

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace GrupoA.Domain.Base.Interface
{
    public interface ISpecification<T>
    {
        Expression<Func<T, bool>> Criteria { get; }
        List<Expression<Func<T, object>>> Includes { get; }
        List<string> IncludeStrings { get; }
        Expression<Func<T, object>> OrderBy { get; }
        Expression<Func<T, object>> OrderByDescending { get; }
        Expression<Func<T, object>> GroupBy { get; }
        Expression<Func<T, object>> Select { get; }
        bool IsCamposPersonalizados { get; }
        int Take { get; }
        int Skip { get; }
        bool IsPaginacaoHabilitada { get; }
    }
}
