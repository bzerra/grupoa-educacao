﻿using System.Collections.Generic;

namespace GrupoA.Domain.Base.Interface
{
    public interface INotifiable<T>
    {
        public IReadOnlyCollection<T> Notifications { get; }
        public bool IsValid { get; }
    }
}
