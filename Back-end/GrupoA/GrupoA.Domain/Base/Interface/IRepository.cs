﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GrupoA.Domain.Base.Interface
{
    public interface IRepository<TEntity>
    {
        #region Add
        Task<TEntity> AddEntity(TEntity obj);
        Task<IEnumerable<TEntity>> AddEntity(IEnumerable<TEntity> listObj);
        #endregion

        #region Update
        Task<TEntity> UpdateEntity(TEntity obj);
        #endregion

        #region Delete
        Task<bool> DeleteEntity(TEntity obj);
        Task<bool> DeleteEntity(IEnumerable<TEntity> listObj);
        #endregion

        #region Get
        Task<TEntity> GetEntityById(Guid id);
        Task<IEnumerable<TEntity>> GetEntity(ISpecification<TEntity> spec = null);
        Task<long> CountAsync(ISpecification<TEntity> spec = null);
        Task<bool> ContainsAsync(ISpecification<TEntity> spec = null);
        Task<bool> ContainsAsync(Func<TEntity,bool> func);
        #endregion
    }
}
