﻿using Flunt.Notifications;
using System;

namespace GrupoA.Domain.Base
{
    public abstract class BaseEntity : Notifiable<Notification>
    {
        protected BaseEntity()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; private set; }
        public bool Updated { get; set; }
    }
}
