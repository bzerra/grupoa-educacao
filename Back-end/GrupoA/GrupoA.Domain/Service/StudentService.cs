﻿using GrupoA.Domain.Base;
using GrupoA.Domain.Base.Interface;
using GrupoA.Domain.Contract.Service;
using GrupoA.Domain.Entity;
using GrupoA.Domain.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrupoA.Domain.Service
{
    public class StudentService : BaseService, IStudentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public StudentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Student> AddStudent(string Name, string Cpf, string Email, string Ra)
        {
            var student = new Student(Name, Cpf, Email, Ra);

            if (student.IsValid is false) {
                AddNotifications(student);
                return null;
            }

            var repositorio = _unitOfWork.Repository<Student>();

            //validation
            await Validation_Student(repositorio, student);

            if (this.Notifications.Any()) return null;

            await repositorio.AddEntity(student);

            if(_unitOfWork.Commit() < 0)
            {
                AddNotification("System", StandardMensage.System_Commit);
                return null;
            }

            return student;
        }

        public async Task<bool> DeleteStudent(Guid Id)
        {
            var repositorio = _unitOfWork.Repository<Student>();

            var student = await repositorio.GetEntityById(Id);

            //validation

            await repositorio.DeleteEntity(student);

            if (_unitOfWork.Commit() < 0) {
                AddNotification("System", StandardMensage.System_Commit);
                return false;
            }

            return true;
        }

        public async Task<Student> UpdateStudent(Guid Id, string Name, string Cpf, string Email, string Ra)
        {
            var repositorio = _unitOfWork.Repository<Student>();


            var student = await repositorio.GetEntityById(Id);
            student.UpdateName(Name).UpdateCpf(Cpf).UpdateEmail(Email).UpdateRa(Ra);

            if (student.IsValid is false) {
                AddNotifications(student);
                return null;
            }

            //validation
            await Validation_Student(repositorio, student);

            if (this.Notifications.Any()) return null;

            await repositorio.UpdateEntity(student);

            if (_unitOfWork.Commit() < 0) {
                AddNotification("System", StandardMensage.System_Commit);
                return null;
            }

            return student;
        }

        #region VALIDATION
        private async Task Validation_Student(IRepository<Student> repositorio, Student student)
        {
            if (await repositorio.ContainsAsync(x => x.Id != student.Id && x.Name.Equals(student.Name)))
                AddNotification("Name", StandardMensage.Student_ExistName);
            if (await repositorio.ContainsAsync(x => x.Id != student.Id && x.Cpf.Equals(student.Cpf)))
                AddNotification("Cpf", StandardMensage.Student_ExistCpf);
            if (await repositorio.ContainsAsync(x => x.Id != student.Id && x.Email.Equals(student.Email)))
                AddNotification("Email", StandardMensage.Student_ExistEmail);
        }
        #endregion
    }
}
