﻿using Flunt.Notifications;
using GrupoA.Domain.Base.Interface;
using GrupoA.Domain.Entity;
using System;
using System.Threading.Tasks;

namespace GrupoA.Domain.Contract.Service
{
    public interface IStudentService : INotifiable<Notification>
    {
        Task<Student> AddStudent(string Name, string Cpf, string Email, string Ra);
        Task<Student> UpdateStudent(Guid Id, string Name, string Cpf, string Email, string Ra);
        Task<bool> DeleteStudent(Guid Id);
    }
}
